# MoCo firmware

MoCo stands for Monitoring and Control board. It communicates with the GBTX chip
in the OBDT board. It runs in the TM7 board hardware. This project is adapted
from the original one ( gitlab.cern.ch/cms-dt-phase2 ) developed by A. Navarro
and J. Sastre, it should implement a GBT bank with two GBT links just to
propagate the clock.

## Structure of the project

Thee project is organized in 3 folders.

### /src/

This is the main sources folder.

For the current version the modified files are in the directory: tm7 and
gbt-fpga .

#### /src/tm7/

The sources specific to tm7, and also some files that needed to be modified from
the original repos (mp7, ipbus...). The original route should be written on the
file to ease backport of changes.

For the current version the modified files are:
moco_gbt/firmware/hdl/top.vhd ,
moco_top/hdl/top.vhd ,
moco_top/ucf/pins.tcl .

#### /src/cactusupgrades/

This folder should be almost a checkout from the cactusupgrades folder from
https://gitlab.cern.ch/cms-cactus/firmware/mp7

The ipbus firmware was originally (in the SVN) hosted together with the rest of
the mp7 packages, but later on, it began being used outside of cms, and even
outside of cern, and because of that, it eventually started to be hosted at
https://github.com/ipbus/ . At early mp7 releases, the ipbus folders are still
at cactusupgrades.

The backport of changes from their original sources at the mp7 and ipbus repos
is not automatic.
The details of the relationship of files used in this project to their original
sources can be found on folder /aux_data/diffs_to_repos.md

For the current version there aren't any modifications in this folder.

#### /src/gbt-fpga/

https://gitlab.cern.ch/gbt-fpga/gbt-fpga heavily modified.

For the current version the modified files are:
example_designs/core_sources/exampleDsgn_package.vhd ,
example_designs/xilinx_k7v7/core_sources/xlx_k7v7_gbt_example_design.vhd ,
example_designs/xilinx_k7v7/vc707/xdc/vc707_gbt_latopt_constraints.tcl .

#### /src/gbt-sc/

Copied without modification from
https://gitlab.cern.ch/gbtsc-fpga-support/gbt-sc

For the current version there aren't any modifications in this folder.

### /vivado/

This folder contains the vivado project itself. The only versioned files here
whould be the vivado project file top.xpr and the final generated bitfile
(top.bit).

There should be no source files in this folder. However, for historical reasons,
there are some IP cores in the /vivado/top/top.srcs/sources_1/ip folder.

For the current version no source files was added in this folder.

### /aux_data/

Other auxiliary data.

For the current version there aren't any modifications in this folder.


