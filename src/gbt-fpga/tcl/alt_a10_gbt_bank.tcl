#===================================================================================================#
#===================================   Srcipt Information   ========================================#
#===================================================================================================#
#                                                                                         
# Company:               CERN (PH-ESE-BE)                                                         
# Engineer:              Julian Mendez <julian.mendez@cern.ch>
#                                                                                                 
# Project Name:          GBT-FPGA                                                                
# Script Name:           Altera Arria 10 - GBT Bank                                        
#                                                                                                 
# Language:              TCL (Altera version)                                                              
#                                                                                                   
# Target Device:         Altera Arria 10                                                      
#                                                                                                   
# Version:               6.0                                                                      
#
# Description:            
#
# Additional Comments:   TCL script for adding the source files of the Altera Arria 10 GBT Bank
#                        to Quartus II
#
#===================================================================================================#
#===================================================================================================#
#===================================================================================================#


#===================================================================================================#
#============================ Absolute Data Path Set By The User ===================================#
#===================================================================================================#

# Comment: The user has to provide the absolute data path to the root folder of the GBT-FPGA Core
#          source files.

set SOURCE_PATH "D:/svn/trunk"


#===================================================================================================#
#=================== Commands for Adding the Source Files of the GBT-FPGA Core =====================#
#===================================================================================================#

# Comment: Adding Common files: 

puts "->" 
puts "-> Adding common files of the GBT-FPGA Core to the Quartus II project..."
puts "->" 

set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_chnsrch.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_deintlver.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_elpeval.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_errlcpoly.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_lmbddet.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_rs2errcor.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_rsdec.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_syndrom.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_descrambler.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_descrambler_16bit.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_descrambler_21bit.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox_latopt.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox_std.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox_std_rdctrl.vhd

set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_tx/gbt_tx.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder_gbtframe_intlver.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder_gbtframe_polydiv.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder_gbtframe_rsencode.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_latopt.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_phasemon.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_std.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_std_rdwrctrl.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_tx/gbt_tx_scrambler.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_tx/gbt_tx_scrambler_16bit.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_tx/gbt_tx_scrambler_21bit.vhd

set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/mgt/mgt_bitslipctrl.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/mgt/mgt_framealigner_pattsearch.vhd

set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_bank.vhd
set_global_assignment -name VHDL_FILE $SOURCE_PATH/gbt_bank/core_sources/gbt_bank_package.vhd


# Comment: Adding Altera Arria 10 specific files:

puts "-> Adding Altera Arria 10 specific files of the GBT-FPGA Core to the Quartus II project..."
puts "->" 

#VHDL Files
set_global_assignment -name VHDL_FILE  $SOURCE_PATH/gbt_bank/altera_a10/alt_ax_gbt_bank_package.vhd
set_global_assignment -name VHDL_FILE  $SOURCE_PATH/gbt_bank/altera_a10/gbt_rx/alt_ax_gbt_rx_gearbox_std_dpram.vhd
set_global_assignment -name VHDL_FILE  $SOURCE_PATH/gbt_bank/altera_a10/gbt_tx/alt_ax_gbt_tx_gearbox_std_dpram.vhd
set_global_assignment -name VHDL_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/alt_ax_mgt.vhd
set_global_assignment -name VHDL_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/alt_ax_mgt_resetctrl.vhd
set_global_assignment -name VHDL_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/alt_ax_mgt_txpll.vhd

#IPs
set_global_assignment -name QSYS_FILE  $SOURCE_PATH/gbt_bank/altera_a10/gbt_rx/alt_ax_rx_dpram/alt_ax_rx_dpram.qsys
set_global_assignment -name QSYS_FILE  $SOURCE_PATH/gbt_bank/altera_a10/gbt_tx/alt_ax_tx_dpram/alt_ax_tx_dpram.qsys
set_global_assignment -name QSYS_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/atx_pll/mgt_atxpll.qsys
set_global_assignment -name QSYS_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/fpll_pll/mgt_fpll.qsys
set_global_assignment -name QSYS_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/gx_reset_rx/gx_reset_rx.qsys
set_global_assignment -name QSYS_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/gx_reset_tx/gx_reset_tx.qsys
set_global_assignment -name QSYS_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/gx_x1/gx_x1.qsys
set_global_assignment -name QSYS_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/gx_x2/gx_x2.qsys
set_global_assignment -name QSYS_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/gx_x3/gx_x3.qsys
set_global_assignment -name QSYS_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/gx_x4/gx_x4.qsys
set_global_assignment -name QSYS_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/gx_x5/gx_x5.qsys
set_global_assignment -name QSYS_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/gx_x6/gx_x6.qsys
set_global_assignment -name QSYS_FILE  $SOURCE_PATH/gbt_bank/altera_a10/mgt/tx_pll_rst/mgt_tx_pll_rst.qsys

