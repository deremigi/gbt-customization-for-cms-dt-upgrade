##===================================================================================================##
##========================= Xilinx design constraints (XDC) information =============================##
##===================================================================================================##
##
## Company:               CERN
## Engineer:              Julian Mendez (julian.mendez@cern.ch)
##
## Project Name:          GBT-FPGA
##
## Target Device:         VC707 (Virtex 7 dev. kit)
## Tool version:          Vivado 2014.4
##
## Version:               4.0
##
## Description:
##
## Versions history:      DATE         VERSION   AUTHOR              DESCRIPTION
##
##                        17/12/2014   1.0       Julian Mendez       First .xdc definition
##
## Additional Comments:
##
##===================================================================================================##
##===================================================================================================##

##===================================================================================================##
##====================================  TIMING CLOSURE  =============================================##
##===================================================================================================##

##=================================================##
## MGT PCS to PMA rxslide constraint               ##
##=================================================##
##set_property RXSLIDE_MODE "PMA" [get_cells -hier -filter {NAME =~ */xlx_k7v7_mgt_ip/xlx_k7v7_mgt_ip_init/xlx_k7v7_mgt_ip_i}]
set_property RXSLIDE_MODE PMA [get_cells -hier -filter {NAME =~ *gbt_inst*gthe2_i}]


