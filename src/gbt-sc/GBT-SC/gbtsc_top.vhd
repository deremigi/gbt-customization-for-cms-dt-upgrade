--============================================================================--
--#######################   Module Information   #############################--
--============================================================================--
--
-- Company          : CERN (EP-ESE-BE)
-- Engineer         : Julian Mendez <julian.mendez@cern.ch>
--
-- Project Name     : GBT-SC module (IC and EC fields)
-- Module Name      : GBTSC_top
--
-- Language         : VHDL
--
-- Target Device    : Device agnostic
-- Tool version     : -
--
-- Version          : 1.0
--
-- Description      : IC and EC control - TOP level
--
-- Versions history : DATE      VERS.   AUTHOR      DESCRIPTION
--
--                    06/04/17  1.0     J. Mendez   First .vhd module definition
--
-- Add. Comments    : -
--                                                                              
--============================================================================--
--############################################################################--
--============================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.SCA_PKG.all;

--============================================================================--
--#############################   Entity   ###################################--
--============================================================================--
entity gbtsc_top is
    generic (
        -- IC configuration
        g_IC_COUNT          : integer := 1;
        g_IC_FIFO_DEPTH     : integer := 20;
        
        -- EC configuration
        g_SCA_COUNT         : integer := 1
    );
    port (
        -- Clock & reset
        tx_clk_i            : in  std_logic;
        rx_clk_i            : in  std_logic;
        reset_i             : in  std_logic;
        
        -- IC configuration        
        tx_GBTx_address_i   : in  reg8_arr((g_IC_COUNT-1) downto 0);
        tx_register_addr_i  : in  reg16_arr((g_IC_COUNT-1) downto 0);
        tx_nb_to_be_read_i  : in  reg16_arr((g_IC_COUNT-1) downto 0);
        
        -- IC Status
        tx_ready_o          : out std_logic_vector((g_IC_COUNT-1) downto 0);
        rx_empty_o          : out std_logic_vector((g_IC_COUNT-1) downto 0);
        
        rx_gbtx_addr_o      : out reg8_arr((g_IC_COUNT-1) downto 0);
        rx_mem_ptr_o        : out reg16_arr((g_IC_COUNT-1) downto 0);
        rx_nb_of_words_o    : out reg16_arr((g_IC_COUNT-1) downto 0);
            
        -- IC FIFO control
        tx_wr_i             : in  std_logic_vector((g_IC_COUNT-1) downto 0);
        tx_data_to_gbtx_i   : in  reg8_arr((g_IC_COUNT-1) downto 0);
        
        rx_rd_i             : in  std_logic_vector((g_IC_COUNT-1) downto 0);
        rx_data_from_gbtx_o : out reg8_arr((g_IC_COUNT-1) downto 0);
        
        -- IC control
        ic_enable_i         : in  std_logic_vector((g_IC_COUNT-1) downto 0);
        tx_start_write_i    : in  std_logic;
        tx_start_read_i     : in  std_logic;
            
        -- SCA control
        sca_enable_i        : in  std_logic_vector((g_SCA_COUNT-1) downto 0);
        start_reset_cmd_i   : in  std_logic;
        start_connect_cmd_i : in  std_logic;
        start_command_i     : in  std_logic;
        inject_crc_error    : in  std_logic;
        
        -- SCA command
        tx_address_i        : in  std_logic_vector(7 downto 0);
        tx_transID_i        : in  std_logic_vector(7 downto 0);
        tx_channel_i        : in  std_logic_vector(7 downto 0);
        --tx_len_i            : in  std_logic_vector(7 downto 0);  -- Not used
        tx_command_i        : in  std_logic_vector(7 downto 0);
        tx_data_i           : in  std_logic_vector(31 downto 0);
        
        rx_received_o       : out std_logic_vector((g_SCA_COUNT-1) downto 0);
        rx_address_o        : out reg8_arr((g_SCA_COUNT-1) downto 0);
        rx_control_o        : out reg8_arr((g_SCA_COUNT-1) downto 0);
        rx_transID_o        : out reg8_arr((g_SCA_COUNT-1) downto 0);
        rx_channel_o        : out reg8_arr((g_SCA_COUNT-1) downto 0);
        rx_len_o            : out reg8_arr((g_SCA_COUNT-1) downto 0);
        rx_error_o          : out reg8_arr((g_SCA_COUNT-1) downto 0);
        rx_data_o           : out reg32_arr((g_SCA_COUNT-1) downto 0);

        -- EC line
        ec_data_o           : out reg2_arr((g_SCA_COUNT-1) downto 0);
        ec_data_i           : in  reg2_arr((g_SCA_COUNT-1) downto 0);
        
        -- IC lines
        ic_data_o           : out reg2_arr((g_IC_COUNT-1) downto 0);
        ic_data_i           : in  reg2_arr((g_IC_COUNT-1) downto 0)
        
    );   
end gbtsc_top;


--============================================================================--
--##########################   Architecture   ################################--
--============================================================================--
architecture behaviour of gbtsc_top is

    -- Signals
    signal tx_start_write_s     : std_logic_vector((g_IC_COUNT-1) downto 0);
    signal tx_start_read_s      : std_logic_vector((g_IC_COUNT-1) downto 0);

    -- Components
    component sca_top is
        generic (
            g_SCA_COUNT         : integer := 1
        );
        port (
            tx_clk_i            : in  std_logic;
            rx_clk_i            : in  std_logic;
            reset_i             : in  std_logic;
            
            enable_i            : in  std_logic_vector(g_SCA_COUNT-1 downto 0);
            start_reset_cmd_i   : in  std_logic;
            start_connect_cmd_i : in  std_logic;
            start_command_i     : in  std_logic;
            inject_crc_error    : in  std_logic;
            
            tx_address_i        : in  std_logic_vector(7 downto 0);
            tx_transID_i        : in  std_logic_vector(7 downto 0);
            tx_channel_i        : in  std_logic_vector(7 downto 0);
            tx_len_i            : in  std_logic_vector(7 downto 0);
            tx_command_i        : in  std_logic_vector(7 downto 0);
            tx_data_i           : in  std_logic_vector(31 downto 0);
            
            rx_received_o       : out std_logic_vector(g_SCA_COUNT-1 downto 0);
            rx_address_o        : out reg8_arr(g_SCA_COUNT-1 downto 0);
            rx_control_o        : out reg8_arr(g_SCA_COUNT-1 downto 0);
            rx_transID_o        : out reg8_arr(g_SCA_COUNT-1 downto 0);
            rx_channel_o        : out reg8_arr(g_SCA_COUNT-1 downto 0);
            rx_len_o            : out reg8_arr(g_SCA_COUNT-1 downto 0);
            rx_error_o          : out reg8_arr(g_SCA_COUNT-1 downto 0);
            rx_data_o           : out reg32_arr(g_SCA_COUNT-1 downto 0);

            tx_data_o           : out reg2_arr(g_SCA_COUNT-1 downto 0);
            rx_data_i           : in  reg2_arr(g_SCA_COUNT-1 downto 0)
            
        );   
    end component sca_top;
    
    component ic_top is
        generic (
            g_FIFO_DEPTH        : integer := 10
        );
        port (
            -- Clock and reset
            tx_clk_i            : in  std_logic;
            rx_clk_i            : in  std_logic;
            reset_i             : in  std_logic;
            
            -- Status
            tx_ready_o          : out std_logic;
            rx_empty_o          : out std_logic;
            
            rx_gbtx_addr_o      : out std_logic_vector(7 downto 0);
            rx_mem_ptr_o        : out std_logic_vector(15 downto 0);
            rx_nb_of_words_o    : out std_logic_vector(15 downto 0);
            
            -- Configuration
            tx_GBTx_address_i   : in  std_logic_vector(7 downto 0);
            tx_register_addr_i  : in  std_logic_vector(15 downto 0);
            tx_nb_to_be_read_i  : in  std_logic_vector(15 downto 0);
            
            -- Internal FIFO
            tx_wr_i             : in  std_logic;
            tx_data_to_gbtx_i   : in  std_logic_vector(7 downto 0); 
            
            rx_rd_i             : in  std_logic;
            rx_data_from_gbtx_o : out std_logic_vector(7 downto 0);
            
            -- FSM Control
            tx_start_write_i    : in  std_logic;
            tx_start_read_i     : in  std_logic;
            
            -- IC lines
            tx_data_o           : out std_logic_vector(1 downto 0);
            rx_data_i           : in  std_logic_vector(1 downto 0)
        );
    end component ic_top;
    
begin           --========####   Architecture Body   ####========-- 


   sca_inst: sca_top
        generic map(
            g_SCA_COUNT         => g_SCA_COUNT
        )
        port map(
            tx_clk_i            => tx_clk_i,
            rx_clk_i            => rx_clk_i,
            reset_i             => reset_i,

            enable_i            => sca_enable_i,
            start_reset_cmd_i   => start_reset_cmd_i,
            start_connect_cmd_i => start_connect_cmd_i,
            start_command_i     => start_command_i,
            inject_crc_error    => inject_crc_error,

            tx_address_i        => tx_address_i,
            tx_transID_i        => tx_transID_i,
            tx_channel_i        => tx_channel_i,
            tx_len_i            => x"04", -- tx_len_i, -- Fixed (BUG)
            tx_command_i        => tx_command_i,
            tx_data_i           => tx_data_i,

            rx_received_o       => rx_received_o,
            rx_address_o        => rx_address_o,
            rx_control_o        => rx_control_o,
            rx_transID_o        => rx_transID_o,
            rx_channel_o        => rx_channel_o,
            rx_len_o            => rx_len_o,
            rx_error_o          => rx_error_o,
            rx_data_o           => rx_data_o,

            tx_data_o           => ec_data_o,
            rx_data_i           => ec_data_i
            
        );   
        
    ic_gen: for i in 0 to (g_IC_COUNT-1) generate
    
        tx_start_write_s(i)     <= tx_start_write_i and ic_enable_i(i);
        tx_start_read_s(i)      <= tx_start_read_i and ic_enable_i(i);
        
        ic_inst: ic_top
            generic map(
                g_FIFO_DEPTH        => g_IC_FIFO_DEPTH
            )
            port map(
                -- Clock and reset
                tx_clk_i            => tx_clk_i,
                rx_clk_i            => rx_clk_i,
                reset_i             => reset_i,

                -- Status
                tx_ready_o          => tx_ready_o(i),
                rx_empty_o          => rx_empty_o(i),

                rx_gbtx_addr_o      => rx_gbtx_addr_o(i),
                rx_mem_ptr_o        => rx_mem_ptr_o(i),
                rx_nb_of_words_o    => rx_nb_of_words_o(i),

                -- Configuration
                tx_GBTx_address_i   => tx_GBTx_address_i(i),
                tx_register_addr_i  => tx_register_addr_i(i),
                tx_nb_to_be_read_i  => tx_nb_to_be_read_i(i),

                -- Internal FIFO
                tx_wr_i             => tx_wr_i(i),
                tx_data_to_gbtx_i   => tx_data_to_gbtx_i(i),

                rx_rd_i             => rx_rd_i(i),
                rx_data_from_gbtx_o => rx_data_from_gbtx_o(i),

                -- FSM Control
                tx_start_write_i    => tx_start_write_s(i),
                tx_start_read_i     => tx_start_read_s(i),

                -- IC lines
                tx_data_o           => ic_data_o(i),
                rx_data_i           => ic_data_i(i)
            );   
    end generate;
    
end behaviour;
--============================================================================--
--############################################################################--
--============================================================================--