--============================================================================--
--#######################   Module Information   #############################--
--============================================================================--
--
-- Company          : CERN (EP-ESE-BE)
-- Engineer         : Julian Mendez <julian.mendez@cern.ch>
--
-- Project Name     : GBT-SC module (IC and EC fields)
-- Module Name      : SCA_rx
--
-- Language         : VHDL
--
-- Target Device    : Device agnostic
-- Tool version     : -
--
-- Version          : 1.0
--
-- Description      : SCA control - SCA RX (HDLC decoder)
--
-- Versions history : DATE      VERS.   AUTHOR      DESCRIPTION
--
--                    05/04/17  1.0     J. Mendez   First .vhd module definition
--
-- Add. Comments    : -
--                                                                              
--============================================================================--
--############################################################################--
--============================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--============================================================================--
--#############################   Entity   ###################################--
--============================================================================--
entity sca_rx is
    port (
        -- Clock & reset
        rx_clk_i        : in std_logic;
        reset_i         : in std_logic;
        
        -- Satus
        done_o          : out std_logic;
        crc_err_o       : out std_logic;    -- Not supported yet
        
        -- RX data
        address_o       : out std_logic_vector(7 downto 0);
        control_o       : out std_logic_vector(7 downto 0);
        transID_o       : out std_logic_vector(7 downto 0);
        channel_o       : out std_logic_vector(7 downto 0);
        len_o           : out std_logic_vector(7 downto 0);
        error_o         : out std_logic_vector(7 downto 0);
        data_o          : out std_logic_vector(31 downto 0);
        
        -- IC Line
        rx_data_i       : in std_logic_vector(1 downto 0)
    );   
end sca_rx;

--============================================================================--
--############################   Architecture   ##############################--
--============================================================================--
architecture behaviour of sca_rx is

    -- Signals
    signal byte_from_des_s      : std_logic_vector(7 downto 0);    
    signal wr_command_s         : std_logic;    
    signal delimiter_detected_s : std_logic;

    signal cmd_v                : std_logic_vector(159 downto 0);

    signal data_word_s2         : std_logic_vector(31 downto 0);
    signal data_word_s4         : std_logic_vector(31 downto 0);
    signal len_s                : std_logic_vector(7 downto 0);
    
    signal fifo_cnt             : std_logic_vector(7 downto 0);
    signal fifo_cnt_i           : integer range 0 to 15;
    signal len_from_fifo        : integer range 0 to 15;
    
    signal rx_received_s        : std_logic;

    -- Components
    component sca_deserializer is
        generic (
            g_WORD_SIZE        : integer := 8;
            g_DELIMITER        : std_logic_vector(7 downto 0) := "01111110";
            g_IDLE             : std_logic_vector(7 downto 0) := "01111111"
        );
        port (
            rx_clk_i           : in  std_logic;
            reset_i            : in  std_logic;

            -- Data
            data_i             : in  std_logic_vector(1 downto 0);
            data_o             : out std_logic_vector((g_WORD_SIZE-1) downto 0);

            -- Control & status
            write_o            : out std_logic;        
            delimiter_found_o  : out std_logic
        );
    end component sca_deserializer;

    component sca_rx_fifo is
        generic (
            g_DEPTH       : integer := 20;
            g_SIZE        : integer := 8
        );
        port (
            rx_clk_i      : in std_logic;
            reset_i       : in std_logic;
            
            -- Data
            data_i        : in  std_logic_vector((g_SIZE-1) downto 0);
            data_o        : out std_logic_vector(((g_SIZE*g_DEPTH)-1) downto 0);

            -- Control
            new_word_i    : in  std_logic;
            write_i       : in  std_logic;
            
            --Status
            to_be_read_o  : out std_logic;
            len_o         : out std_logic_vector(7 downto 0)
        );
    end component sca_rx_fifo;
    
begin          --========####   Architecture Body   ####========-- 

    sca_deserializer_inst: sca_deserializer
       port map(
            rx_clk_i            => rx_clk_i,
            reset_i             => reset_i,
            
            -- Data
            data_i              => rx_data_i,
            data_o              => byte_from_des_s,        
            
            -- Control & status
            write_o             => wr_command_s,
            delimiter_found_o   => delimiter_detected_s
       );

    sca_rx_fifo_inst: sca_rx_fifo
       port map(
            rx_clk_i            => rx_clk_i,
            reset_i             => reset_i,
            
            -- Data
            data_i              => byte_from_des_s,
            data_o              => cmd_v,
            
            -- Control
            write_i             => wr_command_s,
            new_word_i          => delimiter_detected_s,
            
            -- Status
            to_be_read_o        => rx_received_s,
            len_o               => fifo_cnt
       );

    -- Filters SREJ replies
    done_o         <= rx_received_s; -- when cmd_v(8) /= '1' else
                    --'0';

    -- Preparation
    fifo_cnt_i      <= to_integer(unsigned(fifo_cnt));
    
    len_from_fifo   <= fifo_cnt_i - 8 when cmd_v(8) /= '1' and fifo_cnt_i > 8
                       else 0;
    
    len_s           <= std_logic_vector(to_unsigned(len_from_fifo,8));
    --cmd_v(47 downto 40) when cmd_v(8) /= '1' else
    --                   x"00";

    data_word_s2    <= cmd_v(63 downto 56) &    -- Dx
                       cmd_v(55 downto 48) &    -- Dx
                       x"0000";                 -- LSB (x"0000")

    data_word_s4    <= cmd_v(63 downto 56) &    -- Dx
                       cmd_v(55 downto 48) &    -- Dx
                       cmd_v(79 downto 72) &    -- Dx
                       cmd_v(71 downto 64);     -- Dx

    -- Link the fields to the output
    address_o       <= cmd_v(7 downto 0);
    control_o       <= cmd_v(15 downto 8);
    transID_o       <= cmd_v(23 downto 16) when cmd_v(8) /= '1' else x"00";
    channel_o       <= cmd_v(31 downto 24) when cmd_v(8) /= '1' else x"00";
    error_o         <= cmd_v(39 downto 32) when cmd_v(8) /= '1' else x"00";
    len_o           <= std_logic_vector(to_unsigned(len_from_fifo,8));
    data_o          <= data_word_s2 when (len_s = x"01" or len_s = x"02") else
                       data_word_s4 when (len_s = x"03" or len_s = x"04") else
                       x"00000000";

end behaviour;
--============================================================================--
--############################################################################--
--============================================================================--