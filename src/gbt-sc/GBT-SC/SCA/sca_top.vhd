--============================================================================--
--#######################   Module Information   #############################--
--============================================================================--
--
-- Company          : CERN (EP-ESE-BE)
-- Engineer         : Julian Mendez <julian.mendez@cern.ch>
--
-- Project Name     : GBT-SC module (IC and EC fields)
-- Module Name      : SCA_top
--
-- Language         : VHDL
--
-- Target Device    : Device agnostic
-- Tool version     : -
--
-- Version          : 1.0
--
-- Description      : SCA control - SCA top level
--
-- Versions history : DATE      VERS.   AUTHOR      DESCRIPTION
--
--                    06/04/17  1.0     J. Mendez   First .vhd module definition
--
-- Add. Comments    : -
--                                                                              
--============================================================================--
--############################################################################--
--============================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.SCA_PKG.all;

--============================================================================--
--#############################   Entity   ###################################--
--============================================================================--
entity sca_top is
    generic (
        g_SCA_COUNT         : integer := 1
    );
    port (
        tx_clk_i            : in  std_logic;
        rx_clk_i            : in  std_logic;
        reset_i             : in  std_logic;
        
        enable_i            : in  std_logic_vector((g_SCA_COUNT-1) downto 0);
        start_reset_cmd_i   : in  std_logic;
        start_connect_cmd_i : in  std_logic;
        start_command_i     : in  std_logic;
        inject_crc_error    : in  std_logic;
        
        tx_address_i        : in  std_logic_vector(7 downto 0);
        tx_transID_i        : in  std_logic_vector(7 downto 0);
        tx_channel_i        : in  std_logic_vector(7 downto 0);
        tx_len_i            : in  std_logic_vector(7 downto 0);
        tx_command_i        : in  std_logic_vector(7 downto 0);
        tx_data_i           : in  std_logic_vector(31 downto 0);

        rx_received_o       : out std_logic_vector((g_SCA_COUNT-1) downto 0);
        rx_address_o        : out reg8_arr((g_SCA_COUNT-1) downto 0);
        rx_control_o        : out reg8_arr((g_SCA_COUNT-1) downto 0);
        rx_transID_o        : out reg8_arr((g_SCA_COUNT-1) downto 0);
        rx_channel_o        : out reg8_arr((g_SCA_COUNT-1) downto 0);
        rx_len_o            : out reg8_arr((g_SCA_COUNT-1) downto 0);
        rx_error_o          : out reg8_arr((g_SCA_COUNT-1) downto 0);
        rx_data_o           : out reg32_arr((g_SCA_COUNT-1) downto 0);

        tx_data_o           : out reg2_arr((g_SCA_COUNT-1) downto 0);
        rx_data_i           : in  reg2_arr((g_SCA_COUNT-1) downto 0)
        
    );   
end sca_top;


--============================================================================--
--##########################   Architecture   ################################--
--============================================================================--
architecture behaviour of sca_top is

    -- Signals
    signal tx_control_s     : reg8_arr((g_SCA_COUNT-1) downto 0);
    signal rx_control_s     : reg8_arr((g_SCA_COUNT-1) downto 0);
    
    signal start_command_s  : std_logic_vector((g_SCA_COUNT-1) downto 0);
    signal start_connect_s  : std_logic_vector((g_SCA_COUNT-1) downto 0);
    signal start_reset_s    : std_logic_vector((g_SCA_COUNT-1) downto 0);
    
    -- Components
    component sca_rx is
        port (
            -- Clock & reset
            rx_clk_i        : in std_logic;
            reset_i         : in std_logic;
            
            -- Satus
            done_o          : out std_logic;
            crc_err_o       : out std_logic;    -- Not supported yet
            
            -- RX data
            address_o       : out std_logic_vector(7 downto 0);
            control_o       : out std_logic_vector(7 downto 0);
            transID_o       : out std_logic_vector(7 downto 0);
            channel_o       : out std_logic_vector(7 downto 0);
            len_o           : out std_logic_vector(7 downto 0);
            error_o         : out std_logic_vector(7 downto 0);
            data_o          : out std_logic_vector(31 downto 0);
            
            -- IC Line
            rx_data_i       : in std_logic_vector(1 downto 0)
        );   
    end component sca_rx;
    
    component sca_tx is
        port (
            clock           : in  std_logic;
            reset           : in  std_logic;
        
            start_input     : in  std_logic;
            start_connect   : in  std_logic;
            start_reset     : in  std_logic;
        
            address         : in  std_logic_vector(7 downto 0);
            control         : in  std_logic_vector(7 downto 0);
            transID         : in  std_logic_vector(7 downto 0);
            channel         : in  std_logic_vector(7 downto 0);
            len             : in  std_logic_vector(7 downto 0);
            command         : in  std_logic_vector(7 downto 0);
            data            : in  std_logic_vector(31 downto 0);
            data_out        : out std_logic_vector(1 downto 0);

            --Debug
            crc_inj_err     : in  std_logic
        
        );
    end component sca_tx;
    
begin           --========####   Architecture Body   ####========-- 

    sca_gen: for i in 0 to (g_SCA_COUNT-1) generate
                
        start_command_s(i)      <= enable_i(i) and start_command_i;
        start_connect_s(i)      <= enable_i(i) and start_connect_cmd_i;
        start_reset_s(i)        <= enable_i(i) and start_reset_cmd_i;

        -- Time domain crossing (control field)
        ctrl_timedomain_crossing: process(reset_i, tx_clk_i)
        begin
        
            if reset_i = '1' then
                tx_control_s(i)    <= x"00";
            elsif rising_edge(tx_clk_i) then
                tx_control_s(i)    <= rx_control_s(i);
            end if;
            
        end process;
        
        -- TX: The command is sent to all of the SCA enabled
        tx_inst: sca_tx
            port map (
                clock           => tx_clk_i,
                reset           => reset_i,

                start_input     => start_command_s(i),
                start_connect   => start_connect_s(i),
                start_reset     => start_reset_s(i),

                address         => tx_address_i,
                control         => tx_control_s(i),
                transID         => tx_transID_i,
                channel         => tx_channel_i,
                len             => tx_len_i,
                command         => tx_command_i,
                data            => tx_data_i,

                data_out        => tx_data_o(i),
                
                crc_inj_err     => inject_crc_error -- Used for test only
            );
        
        -- RX: individual for each SCA
        rx_inst: sca_rx
            port map(
                rx_clk_i        => rx_clk_i,
                reset_i         => reset_i,
                
                done_o          => rx_received_o(i),
                
                address_o       => rx_address_o(i),
                control_o       => rx_control_s(i),
                transID_o       => rx_transID_o(i),
                channel_o       => rx_channel_o(i),
                len_o           => rx_len_o(i),
                error_o         => rx_error_o(i),
                data_o          => rx_data_o(i),
                
                rx_data_i       => rx_data_i(i)
            );
   
        rx_control_o(i)         <= rx_control_s(i);

    end generate;
    
end behaviour;
--============================================================================--
--############################################################################--
--============================================================================--'