--============================================================================--
--#######################   Module Information   #############################--
--============================================================================--
--
-- Company          : CERN (EP-ESE-BE)
-- Engineer         : Julian Mendez <julian.mendez@cern.ch>
--
-- Project Name     : GBT-SC module (IC and EC fields)
-- Module Name      : SCA_rx_fifo
--
-- Language         : VHDL
--
-- Target Device    : Device agnostic
-- Tool version     : -
--
-- Version          : 1.0
--
-- Description      : SCA control - SCA RX FIFO (Bytes to command word)
--
-- Versions history : DATE      VERS.   AUTHOR      DESCRIPTION
--
--                    07/04/17  1.0     J. Mendez   First .vhd module definition
--
-- Add. Comments    : -
--                                                                              
--============================================================================--
--############################################################################--
--============================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--============================================================================--
--#############################   Entity   ###################################--
--============================================================================--
entity sca_rx_fifo is
    generic (
        g_DEPTH         : integer := 20;
        g_SIZE          : integer := 8
    );
    port (
        rx_clk_i        : in std_logic;
        reset_i         : in std_logic;
        
        -- Data
        data_i          : in  std_logic_vector((g_SIZE-1) downto 0);
        data_o          : out std_logic_vector(((g_SIZE*g_DEPTH)-1) downto 0);

        -- Control
        new_word_i      : in  std_logic;
        write_i         : in  std_logic;
        
        --Status
        to_be_read_o    : out std_logic;
        len_o           : out std_logic_vector(7 downto 0)
    );
end sca_rx_fifo;

--============================================================================--
--############################   Architecture   ##############################--
--============================================================================--
architecture behaviour of sca_rx_fifo is

    -- Types
    subtype reg is std_logic_vector((g_SIZE-1) downto 0);
    type ramreg_t is array(integer range<>) of reg;
    
    -- Signals
    signal mem_arr: ramreg_t(g_DEPTH-1 downto 0);    
    signal wr_ptr: integer range 0 to g_DEPTH-1;
    
begin                 --========####   Architecture Body   ####========-- 

    ram_proc: process(reset_i, rx_clk_i)
    begin

        if reset_i = '1' then
            wr_ptr                  <= 0;
            data_o                  <= (others => '0');
            
            for i in 0 to g_DEPTH-1 loop
                mem_arr(i)          <= (others => '0');
            end loop;
                
        elsif rising_edge(rx_clk_i) then

            to_be_read_o            <= '0';
            
            if write_i = '1' then
                mem_arr(wr_ptr)     <= data_i;
                wr_ptr              <= wr_ptr + 1;
            
            elsif new_word_i = '1' and wr_ptr > 0 then

                to_be_read_o        <= '1';
                len_o               <= std_logic_vector(to_unsigned(wr_ptr,8));
                wr_ptr              <= 0;
                
                for i in 0 to g_DEPTH-1 loop
                    mem_arr(i)      <= (others => '0');
                end loop;
            
                for i in 1 to g_DEPTH loop
                    data_o(((8*i)-1) downto ((i-1)*8))  <= mem_arr(i-1);
                end loop;

            end if;
            
        end if;
        
    end process;

end behaviour;
--============================================================================--
--############################################################################--
--============================================================================--
