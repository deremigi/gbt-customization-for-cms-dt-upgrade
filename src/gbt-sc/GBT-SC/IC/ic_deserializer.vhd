--============================================================================--
--#######################   Module Information   #############################--
--============================================================================--
--
-- Company          : CERN (EP-ESE-BE)
-- Engineer         : Julian Mendez <julian.mendez@cern.ch>
--
-- Project Name     : GBT-SC module (IC and EC fields)
-- Module Name      : IC_deserializer
--
-- Language         : VHDL
--
-- Target Device    : Device agnostic
-- Tool version     : -
--
-- Version          : 1.0
--
-- Description      : GBTx internal control - IC Field deserializer
--
-- Versions history : DATE      VERS.   AUTHOR      DESCRIPTION
--
--                    05/04/17  1.0     J. Mendez   First .vhd module definition
--
-- Add. Comments    : -
--                                                                              
--============================================================================--
--############################################################################--
--============================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--============================================================================--
--#############################   Entity   ###################################--
--============================================================================--
entity ic_deserializer is
    generic (
        g_WORD_SIZE     : integer := 8;
        g_DELIMITER     : std_logic_vector(7 downto 0) := "01111110"
    );
    port (
        clk_i           : in std_logic;
        reset_i         : in std_logic;

        -- Data
        data_i          : in  std_logic_vector(1 downto 0);
        data_o          : out std_logic_vector((g_WORD_SIZE-1) downto 0);

        -- Status
        write_o         : out std_logic;        
        new_word_o      : out std_logic
    );
end ic_deserializer;

--============================================================================--
--##########################   Architecture   ################################--
--============================================================================--
architecture behaviour of ic_deserializer is    
begin                 --========####   Architecture Body   ####========-- 

    deserializer: process(reset_i, clk_i)

        variable reg                : std_logic_vector(g_WORD_SIZE-1 downto 0);
        variable reg_no_destuffing  : std_logic_vector(g_WORD_SIZE-1 downto 0);
        variable cnter              : integer range 0 to g_WORD_SIZE;
        variable ongoing            : std_logic;

    begin

        if reset_i = '1' then
            cnter                   := 0;
            reg                     := (others => '0');
            reg_no_destuffing       := (others => '0');

        elsif rising_edge(clk_i) then

            write_o         <= '0';
            new_word_o        <= '0';

            -- Data(1)
            reg(7 downto 0)     := data_i(1) & reg(7 downto 1);
            reg_no_destuffing   := data_i(1) & reg_no_destuffing(7 downto 1);

            if reg_no_destuffing(7 downto 1) = "0111110" then
                reg(7 downto 0)    := reg(6 downto 0) & '0';

            elsif ongoing  = '1' then
                cnter           := cnter + 1;

            end if;

            if reg_no_destuffing = g_DELIMITER then
                cnter                       := 0;
                ongoing                     := '1';
                new_word_o                  <= '1';

            elsif reg_no_destuffing = x"FF" then
                ongoing                     := '0';

            end if;

            if cnter >= g_WORD_SIZE and ongoing  = '1' then
                cnter                       := 0;
                data_o                      <= reg;
                write_o                     <= '1';
            end if;

            -- Data(0)
            reg(7 downto 0)        := data_i(0) & reg(7 downto 1);
            reg_no_destuffing   := data_i(0) & reg_no_destuffing(7 downto 1);

            if reg_no_destuffing(7 downto 1) = "0111110" then
                reg(7 downto 0)    := reg(6 downto 0) & '0';

            elsif ongoing  = '1' then
                cnter           := cnter + 1;

            end if;

            if reg_no_destuffing = g_DELIMITER then
                cnter                       := 0;
                ongoing                     := '1';
                new_word_o                  <= '1';

            elsif reg_no_destuffing = x"FF" then
                ongoing                     := '0';

            end if;

            if cnter >= g_WORD_SIZE and ongoing  = '1' then
                cnter                       := 0;
                data_o                      <= reg;
                write_o                     <= '1';
            end if;

        end if;
        
    end process;
    
end behaviour;
--============================================================================--
--############################################################################--
--============================================================================--
