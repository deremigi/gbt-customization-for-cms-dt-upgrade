# FILENAME pins.tcl
# Modified for TM7; original in boards/mp7/base_fw/common/firmware/ucf/pins_r1.tcl
# IO pin assignments for the TM7 board
# Clock IO

set_property PACKAGE_PIN L40 [get_ports {clk40_in_n}]
set_property IOSTANDARD LVDS [get_ports {clk40_in_n}]
set_property DIFF_TERM TRUE [get_ports {clk40_in_n}]
set_property PACKAGE_PIN AV38 [get_ports {ttc_in_n}]
set_property IOSTANDARD LVDS [get_ports {ttc_in_n}]
set_property DIFF_TERM TRUE [get_ports {ttc_in_n}]
set_property PACKAGE_PIN AB7 [get_ports eth_clkn]
set_property IOSTANDARD LVCMOS18 [get_ports {clk_100}]
set_property PACKAGE_PIN AJ32 [get_ports {clk_100}]

set_property PACKAGE_PIN M31 [get_ports {clk40pll_n}]
set_property IOSTANDARD LVDS [get_ports {clk40pll_n}]
set_property DIFF_TERM TRUE [get_ports {clk40pll_n}]

# MGT refclk
#set_property PACKAGE_PIN G9 [get_ports {refclkn[0]}]

# Front panel LEDs
set_property SLEW SLOW [get_ports {led*}]
set_property IOSTANDARD LVCMOS18 [get_ports {led*}]
set_property PACKAGE_PIN ah31 [get_ports {led1_yellow}]
set_property PACKAGE_PIN aj31 [get_ports {led1_green}]
set_property PACKAGE_PIN aj35 [get_ports {led1_red}]
set_property PACKAGE_PIN aj33 [get_ports {led1_orange}]
set_property PACKAGE_PIN an34 [get_ports {led2_yellow}]
set_property PACKAGE_PIN an33 [get_ports {led2_green}]
set_property PACKAGE_PIN ap35 [get_ports {led2_red}]
set_property PACKAGE_PIN ap37 [get_ports {led2_orange}]

# Minipod enable
set_property IOSTANDARD LVCMOS33 [get_ports {reset*}]
set_property PACKAGE_PIN bb34 [get_ports {reset_mp_rx_hr}]
set_property PACKAGE_PIN bb31 [get_ports {reset_mp_tx_hr}]

# Geographical address
set_property IOSTANDARD LVCMOS18 [get_ports {GA*}]
set_property PACKAGE_PIN AF36 [get_ports {GA[0]}]
set_property PACKAGE_PIN AF35 [get_ports {GA[1]}]
set_property PACKAGE_PIN AD36 [get_ports {GA[2]}]
set_property PACKAGE_PIN AD37 [get_ports {GA[3]}]

# Dipswitch
set_property IOSTANDARD LVCMOS18 [get_ports {dipswitch*}]
set_property PACKAGE_PIN V30 [get_ports {dipswitch[0]}]
set_property PACKAGE_PIN V31 [get_ports {dipswitch[1]}]
set_property PACKAGE_PIN T29 [get_ports {dipswitch[2]}]
set_property PACKAGE_PIN T30 [get_ports {dipswitch[3]}]
set_property PACKAGE_PIN W30 [get_ports {dipswitch[4]}]
set_property PACKAGE_PIN W31 [get_ports {dipswitch[5]}]
set_property PACKAGE_PIN V29 [get_ports {dipswitch[6]}]
set_property PACKAGE_PIN U29 [get_ports {dipswitch[7]}]

# Readout pins
#set_property PACKAGE_PIN AH28 [get_ports SEL1_MGT_CLK_18]
#set_property IOSTANDARD LVCMOS18 [get_ports SEL1_MGT_CLK_18]

# GBT BANK
# MGTREFCLK0P_117
set_property PACKAGE_PIN K8 [get_ports gbt_clkp] 
set_property PACKAGE_PIN K7 [get_ports gbt_clkn]
# MGTHTXP0_117
# De Remigis
set_property PACKAGE_PIN N2 [get_ports gbt_txp]  
set_property PACKAGE_PIN N1 [get_ports gbt_txn]
set_property PACKAGE_PIN P8 [get_ports gbt_rxp]
set_property PACKAGE_PIN P7 [get_ports gbt_rxn]
set_property PACKAGE_PIN M4 [get_ports gbt_txp2]
set_property PACKAGE_PIN M3 [get_ports gbt_txn2]
set_property PACKAGE_PIN N6 [get_ports gbt_rxp2]
set_property PACKAGE_PIN N5 [get_ports gbt_rxn2]
