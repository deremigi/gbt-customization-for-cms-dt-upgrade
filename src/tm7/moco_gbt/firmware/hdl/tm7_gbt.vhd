-- FILENAME tm7_gbt.vhd
-- VERSION 0
-- DATE 10.07.19
-- COMMENT Module for GBT implementation adapted from A. Navarro and J. Sastre
-- CONTACT P. De Remigis, F. Rotondo
-- Module for TM7 communication with the GBTX chip             
-- Adapted from https://gitlab.cern.ch/gbtsc-fpga-support/gbtsc-example-design
-- By Javier Sastre Alvaro (javier.sastre@ciemat.es) & Alvaro Navarro Tobar
-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

-- Custom libraries and packages:
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_exampledesign_package.all;
use work.tm7_gbt_decl.all;
use work.bo2.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity tm7_gbt is
  port (   
    --CPU_RESET                                      : in  std_logic;     
    clk40                          : in std_logic; -- Frame clock
    -- vc707_gbt_example_design uses 156 MHz for DRP/SYSCLK, we replace it by eth_clk at 125 MHz 
    clk_fr                         : in std_logic; -- Fabric clock 125 MHz   
    -- MGT(GTX) reference clock (from external Si5338 clock generator), 120MHz for the latency-optimized GBT Bank
    gbt_clkp, gbt_clkn             : in  std_logic;
    -- txFrameClk locked
    txFrameClk_lock                : out std_logic;   
-- MGT(GTX) --
-- To be modified: SFP should be MGTHTXP0_117
-- De Remigis
	gbt_txp,	gbt_txn:	out	Std_logic;
	gbt_rxp,	gbt_rxn:	in	Std_logic;
	gbt_txp2,	gbt_txn2:	out	Std_logic;
	gbt_rxp2,	gbt_rxn2:	in	Std_logic;
-- ipbus --
    ipb_clk           : in std_logic;
    ipb_rst           : in std_logic;
    ipb_in            : in ipb_wbus;
    ipb_out           : out ipb_rbus;
    
    ----------------192.135.19.96
    -- data ports --
    ----------------
    bunch_ctr   : in std_logic_vector(11 downto 0);
    rxFrameClk  : out std_logic;
    rxData      : out std_logic_vector(83 downto 0);
    headerLock  : out std_logic
    
  );
end tm7_gbt;

architecture structural of tm7_gbt is

  --================================ Signal Declarations ================================--          
  --===============--     
  -- General reset --     
  --===============--     
  signal reset_from_genRst                          : std_logic;    
  
  --===============--
  -- Clocks scheme -- 
  --===============--   
  -- MGT(GTX) reference clock:     
  signal mgtRefClk_from_IbufdsGtxe2                : std_logic;
  -- Frame clock:
  signal txFrameClk_from_txPll                    : std_logic;
  signal txFrameClk_lock_from_txPll               : std_logic;
   
  --=========================--
  -- GBT Bank example design --
  --=========================--
  
  -- Control:
  -----------
  signal txPllReset                                 : std_logic;
  
  signal generalReset_from_user                     : std_logic;   
  signal resetgbtfpga_from_jtag                     : std_logic;
  signal resetgbtfpga_from_vio                      : std_logic;
     
  signal manualResetTx_from_user                    : std_logic; 
  signal manualResetRx_from_user                    : std_logic; 
  signal testPatterSel_from_user                    : std_logic_vector(1 downto 0); 
  signal loopBack_from_user                         : std_logic_vector(2 downto 0); 
  signal resetDataErrorSeenFlag_from_user           : std_logic; 
  signal resetGbtRxReadyLostFlag_from_user          : std_logic; 
  signal txIsDataSel_from_user                      : std_logic;
  signal rxBitSlipRstCount_from_gbtExmplDsgn        : std_logic_vector(7 downto 0);
     
  --------------------------------------------------      
  signal latOptGbtBankTx_from_gbtExmplDsgn          : std_logic;
  signal latOptGbtBankRx_from_gbtExmplDsgn          : std_logic;
  signal txFrameClkPllLocked_from_gbtExmplDsgn      : std_logic;
  signal rxFrameClkReady_from_gbtExmplDsgn          : std_logic; 
  signal gbtRxReady_from_gbtExmplDsgn               : std_logic;    
  signal rxIsData_from_gbtExmplDsgn                 : std_logic;        
-- Data:
-- De Remigis
	signal	txdata_from_gbtexmpldsgn,
		txdata_from_gbtexmpldsgn2,
		rxdata_from_gbtexmpldsgn,
		rxdata_from_gbtexmpldsgn2,
		txdata_to_gbtexmpldsgn,
		txdata_to_gbtexmpldsgn2:	Std_logic_vector(83 downto 0);
	signal	mgt_rxword_s,
		mgt_rxword_rev_s,
		mgt_txword_s,
		mgt_txword_rev_s:Std_logic_vector(39 downto	0);
	signal	gbt_txencdata_s,gbt_rxencdata_s:Std_logic_vector(119 downto 0);
	signal	mgt_headerlocked_s,	mgt_headerlocked2_s:	Std_logic;
  -- BER                 --
	signal	modifiedbitscnt:	Std_logic_vector(7	downto	0);
	signal	countwordreceived,
		countbitsmodified,
		countworderrors:		Std_logic_vector(31 downto 0);
	signal	gbtmodifiedbitflagfiltered:	Std_logic_vector(127 downto 0);
	signal	gbterrordetected:		Std_logic;
	signal	gbtmodifiedbitflag:		Std_logic_vector(83 downto 0);
  -- Latency measurement --
  --=====================--
  signal shiftTxClock_from_vio            : std_logic;
  signal txShiftCount_from_vio             : std_logic_vector(7 downto 0);
  signal DEBUG_CLK_ALIGNMENT_debug                  : std_logic_vector(2 downto 0);
  signal rxBitSlipRstOnEven_from_user              : std_logic;
  signal txAligned_from_gbtbank            : std_logic;
  signal txAlignComputed_from_gbtbank      : std_logic;
  signal txFrameClk_from_gbtExmplDsgn               : std_logic;
  signal txWordClk_from_gbtExmplDsgn                : std_logic;
  signal rxFrameClk_from_gbtExmplDsgn               : std_logic;
  signal rxWordClk_from_gbtExmplDsgn                : std_logic;
  --------------------------------------------------                                    
  signal txMatchFlag_from_gbtExmplDsgn              : std_logic;
  
  signal txEncodingSel                              : std_logic;
  signal rxEncodingSel                              : std_logic;
-- ILA component  --
-- Vivado synthesis tool does not support mixed-language
-- Solution: http://www.xilinx.com/support/answers/47454.html
-- De Remigis
component	xlx_k7v7_vivado_debug	port
	(clk:		in	Std_logic;
	probe0,	probe1:	in	Std_logic_vector(1	downto	0);
	probe2:		in	Std_logic_vector(79	downto	0);
	probe3:		in	Std_logic_vector(1	downto	0);
	probe4:		in	Std_logic_vector(79	downto	0));
  END COMPONENT;

  -- Jtag to Axi component and signals:
  --     Used to control the design and monitor the signals in order to
  --     perform automatic tests.
  signal m_axi_awaddr 		:  STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal m_axi_awprot         :  STD_LOGIC_VECTOR(2 DOWNTO 0);
  signal m_axi_awvalid         :  STD_LOGIC;
  signal m_axi_awready         :  STD_LOGIC;
  signal m_axi_wdata         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal m_axi_wstrb         :  STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal m_axi_wvalid         :  STD_LOGIC;
  signal m_axi_wready         :  STD_LOGIC;
  signal m_axi_bresp         :  STD_LOGIC_VECTOR(1 DOWNTO 0);
  signal m_axi_bvalid         :  STD_LOGIC;
  signal m_axi_bready         :  STD_LOGIC;
  signal m_axi_araddr         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal m_axi_arprot         :  STD_LOGIC_VECTOR(2 DOWNTO 0);
  signal m_axi_arvalid         :  STD_LOGIC;
  signal m_axi_arready         :  STD_LOGIC;
  signal m_axi_rdata         :  STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal m_axi_rresp         :  STD_LOGIC_VECTOR(1 DOWNTO 0);
  signal m_axi_rvalid         :  STD_LOGIC;
  signal m_axi_rready         :  STD_LOGIC;
   
  
  -- IC Debug
  ------------
  signal GBTx_address_to_gbtic, GBTx_address_to_gbtic_jtag      : std_logic_vector(7 downto 0);
  signal Register_addr_to_gbtic, Register_addr_to_gbtic_jtag     : std_logic_vector(15 downto 0);
  signal nb_to_be_read_to_gbtic, nb_to_be_read_to_gbtic_jtag     : std_logic_vector(15 downto 0);
  signal start_write_to_gbtic, start_write_to_gbtic_jtag       : std_logic;
  signal start_read_to_gbtic, start_read_to_gbtic_jtag        : std_logic;
  signal data_to_gbtic, data_to_gbtic_jtag              : std_logic_vector(7 downto 0);
  signal wr_to_gbtic, wr_to_gbtic_jtag                : std_logic;
  signal rd_to_gbtic, rd_to_gbtic_jtag                : std_logic;
 
  --SCA debug
  -----------
  signal reset_sca, reset_sca_jtag: std_logic;
  signal sca_ready: std_logic;
  signal start_sca, start_sca_jtag: std_logic;
  signal start_reset, start_reset_jtag: std_logic;
  signal start_connect, start_connect_jtag: std_logic;
  
  signal sca_rx_parr: std_logic_vector(159 downto 0);
  signal sca_rx_done: std_logic;
  
  signal tx_ready: std_logic;
  signal tx_addr: std_logic_vector(7 downto 0);
  signal tx_ctrl: std_logic_vector(7 downto 0);
  signal tx_trid, tx_trid_jtag: std_logic_vector(7 downto 0);
  signal tx_ch, tx_ch_jtag: std_logic_vector(7 downto 0);
  signal tx_len, tx_len_jtag: std_logic_vector(7 downto 0);
  signal tx_cmd, tx_cmd_jtag: std_logic_vector(7 downto 0);
  signal tx_data, tx_data_jtag: std_logic_vector(31 downto 0);
  
  signal rx_addr: std_logic_vector(7 downto 0);
  signal rx_ctrl: std_logic_vector(7 downto 0);
  signal rx_ch: std_logic_vector(7 downto 0);
 
  signal m_axi_awaddr_to_gbtic : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal m_axi_awprot_to_gbtic : STD_LOGIC_VECTOR(2 DOWNTO 0);
  signal m_axi_awvalid_to_gbtic : STD_LOGIC;
  signal m_axi_awready_to_gbtic : STD_LOGIC;
  signal m_axi_wdata_to_gbtic : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal m_axi_wstrb_to_gbtic : STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal m_axi_wvalid_to_gbtic : STD_LOGIC;
  signal m_axi_wready_to_gbtic : STD_LOGIC;
  signal m_axi_bresp_to_gbtic : STD_LOGIC_VECTOR(1 DOWNTO 0);
  signal m_axi_bvalid_to_gbtic : STD_LOGIC;
  signal m_axi_bready_to_gbtic : STD_LOGIC;
  signal m_axi_araddr_to_gbtic : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal m_axi_arprot_to_gbtic : STD_LOGIC_VECTOR(2 DOWNTO 0);
  signal m_axi_arvalid_to_gbtic : STD_LOGIC;
  signal m_axi_arready_to_gbtic : STD_LOGIC;
  signal m_axi_rdata_to_gbtic : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal m_axi_rresp_to_gbtic : STD_LOGIC_VECTOR(1 DOWNTO 0);
  signal m_axi_rvalid_to_gbtic : STD_LOGIC;
  signal m_axi_rready_to_gbtic : STD_LOGIC;
  
  signal m_axi_awaddr_to_gbtsc : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal m_axi_awprot_to_gbtsc : STD_LOGIC_VECTOR(2 DOWNTO 0);
  signal m_axi_awvalid_to_gbtsc : STD_LOGIC;
  signal m_axi_awready_to_gbtsc : STD_LOGIC;
  signal m_axi_wdata_to_gbtsc : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal m_axi_wstrb_to_gbtsc : STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal m_axi_wvalid_to_gbtsc : STD_LOGIC;
  signal m_axi_wready_to_gbtsc : STD_LOGIC;
  signal m_axi_bresp_to_gbtsc : STD_LOGIC_VECTOR(1 DOWNTO 0);
  signal m_axi_bvalid_to_gbtsc : STD_LOGIC;
  signal m_axi_bready_to_gbtsc : STD_LOGIC;
  signal m_axi_araddr_to_gbtsc : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal m_axi_arprot_to_gbtsc : STD_LOGIC_VECTOR(2 DOWNTO 0);
  signal m_axi_arvalid_to_gbtsc : STD_LOGIC;
  signal m_axi_arready_to_gbtsc : STD_LOGIC;
  signal m_axi_rdata_to_gbtsc : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal m_axi_rresp_to_gbtsc : STD_LOGIC_VECTOR(1 DOWNTO 0);
  signal m_axi_rvalid_to_gbtsc : STD_LOGIC;
  signal m_axi_rready_to_gbtsc : STD_LOGIC;
          
  signal fast_clock_for_meas  : std_logic;
  signal cmd_delay            : std_logic_vector(31 downto 0);
       
  attribute mark_debug : string;
  
  attribute mark_debug of rxIsData_from_gbtExmplDsgn: signal is "TRUE";
  attribute mark_debug of latOptGbtBankTx_from_gbtExmplDsgn: signal is "TRUE";
  attribute mark_debug of rxFrameClkReady_from_gbtExmplDsgn: signal is "TRUE";
  attribute mark_debug of gbtRxReady_from_gbtExmplDsgn: signal is "TRUE";
  attribute mark_debug of latOptGbtBankRx_from_gbtExmplDsgn: signal is "TRUE";
  attribute mark_debug of generalReset_from_user: signal is "TRUE";
  attribute mark_debug of testPatterSel_from_user: signal is "TRUE";
  attribute mark_debug of loopBack_from_user: signal is "TRUE";
  attribute mark_debug of resetDataErrorSeenFlag_from_user: signal is "TRUE";
  attribute mark_debug of resetGbtRxReadyLostFlag_from_user: signal is "TRUE";
  attribute mark_debug of txIsDataSel_from_user: signal is "TRUE";
  attribute mark_debug of manualResetTx_from_user: signal is "TRUE";
  attribute mark_debug of manualResetRx_from_user: signal is "TRUE";
  --=====================================================================================--  

  -- IPBUS REGISTER
-- De Remigis
constant	N_STAT:	Integer:=	128;
constant	N_CTRL:	Integer:=	64;
signal	ctrl_stb:	Std_logic_vector(N_CTRL	-1	downto	0);
signal	stat,	csrd:	Ipb_reg_v	(N_STAT	-1	downto	0);
signal	conf,	csrq:	Ipb_reg_v	(N_CTRL	-1	downto	0);
signal	reset_reply_flag_s,
	reset_reply_flag_f1,
	reset_reply_flag_f2,
	reset_reply_flag_and:	Std_logic:=	'0';
-- Start of automatically-generated VHDL code for register "breakout" signals
-- declaration
-- De Remigis
signal	conf_elink_serial_fire,
	conf_gbtsc_intf,
	conf_ic_rd_wr,
	conf_manualresetrx,
	conf_manualresettx,
	conf_read_fifo,
	conf_reset_gbt_sc,
	conf_resetgbtfpga,
	conf_send_connect,
	conf_send_reset,
	conf_txisdatasel,
	stat_link_ready,
	stat_link_ready2,
	stat_ready,
	stat_reply_flag,
	stat_rx_data_error_seen,
	stat_rx_empty,
	stat_rx_error_detected,
	stat_rx_frame_ready,
	stat_rx_gbt_ready,
	stat_rx_gbt_ready_lost_flag,
	stat_rx_is_data,
	stat_rx_match_flag,
	stat_tx_aligned,
	stat_tx_match_flag:	Std_logic:=	'0';
signal	conf_loopback,
	conf_test_pattern_sel,
	conf_txdata_sel:Std_logic_vector(1	downto	0):=	(others=>'0');
signal	conf_ch,
	conf_comm,
	conf_gbtx_addr,
	conf_ic_data,
	conf_id,
	conf_len,
	stat_ch,stat_error,
	stat_gbtx_addr,
	stat_ic_data,
	stat_id,
	stat_len:	Std_logic_vector(7	downto	0):=	(others=>'0');
signal	conf_bunchctr_fire:	Std_logic_vector(11 downto 0):=(others=>'0');
signal	conf_gbt_data_group_0,
	conf_gbt_data_group_1,
	conf_gbt_data_group_2,
	conf_gbt_data_group_3,
	conf_gbt_data_group_4,
	conf_nb_to_read,
	conf_reg,
	stat_nb,
	stat_reg:	Std_logic_vector(15	downto	0):=	(others=>'0');
signal	conf_ec_data,stat_ec_data:Std_logic_vector(31 downto 0):=(others=>'0');
-- End of automatically-generated VHDL code for register "breakout" signals
-- declaration									
-- Start of automatically-generated VHDL code for register "breakout" signals
-- assignment
-- De Remigis
begin	conf_resetgbtfpga<=			conf	(16#00#)(0);
	conf_manualresettx<=			conf	(16#00#)(1);
	conf_manualresetrx<=	 		conf	(16#00#)(2);
	conf_loopback<=				conf	(16#00#)(4 downto 3);
	conf_txisdatasel<=			conf	(16#00#)(5);
	conf_test_pattern_sel<=			conf	(16#00#)(7 downto 6);
	conf_gbtsc_intf<=			conf	(16#00#)(8);
	conf_txdata_sel<=			conf	(16#00#)(10 downto 9);
	conf_elink_serial_fire<=		conf	(16#00#)(11);
	conf_bunchctr_fire<=			conf	(16#00#)(23 downto 12);
	conf_gbt_data_group_0<=			conf	(16#01#)(15 downto 0);
	conf_gbt_data_group_1<=			conf	(16#01#)(31 downto 16);
	conf_gbt_data_group_2<=			conf	(16#02#)(15 downto 0);
	conf_gbt_data_group_3<=			conf	(16#02#)(31 downto 16);
	conf_gbt_data_group_4<=			conf	(16#03#)(15 downto 0);
	conf_id<=				conf	(16#04#)(7 downto 0);
	conf_ch<=				conf	(16#04#)(15 downto 8);
	conf_len<=				conf	(16#04#)(23 downto 16);
	conf_comm<=				conf	(16#04#)(31 downto 24);
	conf_ec_data<=				conf	(16#05#)(31 downto 0);
	conf_send_reset<=			conf	(16#06#)(0);
	conf_send_connect<=			conf	(16#06#)(1);
	conf_reset_gbt_sc<=			conf	(16#06#)(2);
	conf_gbtx_addr<=			conf	(16#07#)(7 downto 0);
	conf_ic_rd_wr<=				conf	(16#07#)(8);
	conf_read_fifo<=			conf	(16#07#)(9);
	conf_nb_to_read<=			conf	(16#07#)(31 downto 16);
	conf_reg<=				conf	(16#08#)(15 downto 0);
	conf_ic_data<=				conf	(16#09#)(7 downto 0);
	stat	(16#00#)(7	downto	0)<=	stat_id;
	stat	(16#00#)(15	downto	8)<=	stat_ch;
	stat	(16#00#)(23	downto	16)<=	stat_len;
	stat	(16#00#)(31	downto	24)<=	stat_error;
	stat	(16#01#)(31	downto	0)<=	stat_ec_data;
	stat	(16#02#)(0)<=			stat_reply_flag;
	stat	(16#02#)(1)<=			stat_ready;
	stat	(16#02#)(2)<=			stat_rx_empty;
	stat	(16#03#)(7	downto	0)<=	stat_gbtx_addr;
	stat	(16#04#)(15	downto	0)<=	stat_reg;
	stat	(16#04#)(31	downto	16)<=	stat_nb;
	stat	(16#05#)(7	downto	0)<=	stat_ic_data;
	stat	(16#06#)(0)<=			stat_link_ready;
	stat	(16#06#)(1)<=			stat_tx_match_flag;
	stat	(16#06#)(2)<=			stat_tx_aligned;
	stat	(16#06#)(4)<=			stat_rx_gbt_ready;
	stat	(16#06#)(5)<=			stat_rx_gbt_ready_lost_flag;
	stat	(16#06#)(6)<=			stat_rx_data_error_seen;
	stat	(16#06#)(7)<=			stat_rx_match_flag;
	stat	(16#06#)(8)<=			stat_rx_is_data;
	stat	(16#06#)(9)<=			stat_rx_error_detected;
	stat	(16#06#)(10)<=			stat_rx_frame_ready;
	stat	(26)	(0)<=			stat_link_ready2;									
-- End of automatically-generated VHDL code for register "breakout" signals
-- assignment
  stat_rx_gbt_ready         <= gbtRxReady_from_gbtExmplDsgn;
  stat_rx_is_data           <= rxIsData_from_gbtExmplDsgn;
  stat_rx_error_detected    <= gbtErrorDetected;
  
--------------------------------------
--------------------------------------
-- IPbus register
--------------------------------------
--------------------------------------

sync_reg: entity work.ipbus_syncreg_v
  generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
  port map(
    clk => ipb_clk,
    rst => ipb_rst,
    ipb_in => ipb_in,
    ipb_out => ipb_out,
    slv_clk => txFrameClk_from_txPll,
    d => csrd,
    q => csrq,
    --rstb => stat_stb,
    stb => ctrl_stb        );

conf <= csrq;
csrd <= stat;

resetgbtfpga_from_vio <= conf_resetgbtfpga;
testPatterSel_from_user <= conf_test_pattern_sel;
loopBack_from_user(1 downto 0) <= conf_loopback;
loopBack_from_user(2) <= '0';
txIsDataSel_from_user <= conf_txIsDataSel;
manualResetTx_from_user <= conf_manualResetTx;
manualResetRx_from_user <= conf_manualResetRx;



--=========--
-- TX data -- 
-- De Remigis
	datagbt1:	process	(txframeclk_from_txpll)
  variable pipeout : std_logic_vector(31 downto 0);
  variable elink_fire_old : std_logic;
begin
  if rising_edge(txFrameClk_from_txPll) then
    if conf_txdata_sel = "00" then
      txData_to_gbtExmplDsgn(79 downto 8) <= (others => '0');
      txData_to_gbtExmplDsgn(6 downto 1) <= conf_gbt_data_group_4(6 downto 1);
      txData_to_gbtExmplDsgn(0) <= bo2sl(bunch_ctr = conf_bunchctr_fire);

      if (elink_fire_old = '0' and conf_elink_serial_fire = '1') then
        pipeout := conf_gbt_data_group_1 & conf_gbt_data_group_0 ; 
      end if; 
      txData_to_gbtExmplDsgn(7) <= pipeout(31);
    elsif conf_txdata_sel = "01" then
      txData_to_gbtExmplDsgn(79 downto 0) <= conf_gbt_data_group_4 & conf_gbt_data_group_3 & conf_gbt_data_group_2 & conf_gbt_data_group_1 & conf_gbt_data_group_0;
    end if;
    
    pipeout := pipeout(30 downto 0) & '0';
    elink_fire_old := conf_elink_serial_fire ;
  end if;
end process;
-- De Remigis
	datagbt2:	process	(txframeclk_from_txpll)
		variable	pipeout:Std_logic_vector(31	downto	0);
		variable	elink_fire_old:	Std_logic;
	begin	if	rising_edge	(txframeclk_from_txpll)	then
			if	conf_txdata_sel="00"	then
	txdata_to_gbtexmpldsgn2(79 downto 8)<=(others=>'0');
	txdata_to_gbtexmpldsgn2(6 downto 1)<=conf_gbt_data_group_4(6 downto 1);
	txdata_to_gbtexmpldsgn2(0)<=bo2sl(bunch_ctr=conf_bunchctr_fire);
			if(elink_fire_old='0'and conf_elink_serial_fire='1')
		then pipeout:=conf_gbt_data_group_1&conf_gbt_data_group_0;
			end	if;
			txdata_to_gbtexmpldsgn2	(7)<=	pipeout	(31);
			elsif conf_txdata_sel="01"
				then txdata_to_gbtexmpldsgn2(79 downto 0)<=
					conf_gbt_data_group_4
					&conf_gbt_data_group_3
					&conf_gbt_data_group_2
					&conf_gbt_data_group_1
					&conf_gbt_data_group_0;
			end	if;
			pipeout:=	pipeout	(30	downto	0)	&'0';
			elink_fire_old:=conf_elink_serial_fire;
		end	if;
	end process;
-- General reset -- 
generalreset_from_user<=resetgbtfpga_from_jtag or resetgbtfpga_from_vio;
genRst: entity work.xlx_k7v7_reset
  generic map (
    CLK_FREQ    => 25e3)
  port map (    
    CLK_I       => txFrameClk_from_txPll, 
    RESET1_B_I  => not generalReset_from_user, 
    RESET2_B_I  => not generalReset_from_user,
    RESET_O     => reset_from_genRst 
  ); 

-- MGT(GTX) reference clock:
----------------------------

-- Comment: * The MGT reference clock MUST be provided by an external clock generator.
--          * The MGT reference clock frequency must be 120MHz for the latency-optimized GBT Bank. 

mgtRefClk_ge: ibufds_gte2
  port map (
    O       => mgtRefClk_from_IbufdsGtxe2,
    ODIV2   => open,
    CEB     => '0',
    I       => gbt_clkp,
    IB      => gbt_clkn
  );
   
txFrameclkGen_inst: entity work.xlx_k7v7_tx_phaligner
  Port map( 
    -- Reset
    RESET_IN              => txPllReset,
    -- Clocks
    CLK_IN                => clk40, -- JSA --
    CLK_OUT               => txFrameClk_from_txPll,  -- JSA -- PLL bypass     
    -- Control
    SHIFT_IN              => shiftTxClock_from_vio,
    SHIFT_COUNT_IN        => txShiftCount_from_vio,
    -- Status
    LOCKED_OUT            => txFrameClk_lock_from_txPll
 );

 txFrameClk_lock <= txFrameClk_lock_from_txPll;
  
--=========================--
-- GBT Bank example design --
--=========================--	
gbtExmplDsgn_inst: entity work.xlx_k7v7_gbt_example_design
  generic map(
    NUM_LINKS                                      => NUM_LINK_Conf,                 -- Up to 4
    TX_OPTIMIZATION                                => TX_OPTIMIZATION_Conf,          -- LATENCY_OPTIMIZED or STANDARD
    RX_OPTIMIZATION                                => RX_OPTIMIZATION_Conf,          -- LATENCY_OPTIMIZED or STANDARD
    TX_ENCODING                                    => TX_ENCODING_Conf,         -- GBT_FRAME or WIDE_BUS
    RX_ENCODING                                    => RX_ENCODING_Conf,         -- GBT_FRAME or WIDE_BUS
    
    DATA_GENERATOR_ENABLE                          => DATA_GENERATOR_ENABLE_Conf,
    DATA_CHECKER_ENABLE                            => DATA_CHECKER_ENABLE_Conf,
    MATCH_FLAG_ENABLE                              => MATCH_FLAG_ENABLE_Conf,
    CLOCKING_SCHEME                                => CLOCKING_SCHEME_Conf
  )
  port map (
-- Clocks       --
-- De Remigis
		frameclk_40mhz=>	txframeclk_from_txpll,
		xcvrclk=>		mgtrefclk_from_ibufdsgtxe2,
		tx_frameclk_o	(1)=>	txframeclk_from_gbtexmpldsgn,
		tx_frameclk_o	(2)=>	open,
		tx_wordclk_o	(1)=>	txwordclk_from_gbtexmpldsgn,
		tx_wordclk_o	(2)=>	open,
		rx_frameclk_o	(1)=>	rxframeclk_from_gbtexmpldsgn,
		rx_frameclk_o	(2)=>	open,
		rx_wordclk_o	(1)=>	rxwordclk_from_gbtexmpldsgn,
		rx_wordclk_o	(2)=>	open,
-- Reset        --
-- De Remigis
		gbtbank_general_reset_i=>	reset_from_genrst,
		gbtbank_manual_reset_tx_i=>	manualresettx_from_user,
		gbtbank_manual_reset_rx_i=>	manualresetrx_from_user,
-- Serial lanes --
-- De Remigis
		gbtbank_mgt_rx_p	(1)=>	gbt_rxp,	
		gbtbank_mgt_rx_p	(2)=>	gbt_rxp2,	
		gbtbank_mgt_rx_n	(1)=>	gbt_rxn,	
		gbtbank_mgt_rx_n	(2)=>	gbt_rxn2,	
		gbtbank_mgt_tx_p	(1)=>	gbt_txp,	
		gbtbank_mgt_tx_p	(2)=>	gbt_txp2,	
		gbtbank_mgt_tx_n	(1)=>	gbt_txn,	
		gbtbank_mgt_tx_n	(2)=>	gbt_txn2,	
-- Data             --
-- De Remigis
		gbtbank_gbt_data_i	(1)=>	txdata_to_gbtexmpldsgn,
		gbtbank_gbt_data_i	(2)=>	txdata_to_gbtexmpldsgn2,
		gbtbank_wb_data_i	(1)=>	(others=>	'0'),
		gbtbank_wb_data_i	(2)=>	(others=>	'0'),
		tx_data_o		(1)=>	txdata_from_gbtexmpldsgn,
		tx_data_o		(2)=>	txdata_from_gbtexmpldsgn2,
		wb_data_o		(1)=>	open,
		wb_data_o		(2)=>	open,
		gbtbank_gbt_data_o	(1)=>	rxdata_from_gbtexmpldsgn,
		gbtbank_gbt_data_o	(2)=>	rxdata_from_gbtexmpldsgn2,
		gbtbank_wb_data_o	(1)=>	open,
		gbtbank_wb_data_o	(2)=>	open,
		mgt_rxword_o		(1)=>	mgt_rxword_s,
		mgt_rxword_o		(2)=>	open,
		mgt_txword_o		(1)=>	mgt_txword_s,
		mgt_txword_o		(2)=>	open,
		gbt_txencdata_o		(1)=>	gbt_txencdata_s,
		gbt_txencdata_o		(2)=>	open,
		gbt_rxencdata_o		(1)=>	gbt_rxencdata_s,
		gbt_rxencdata_o		(2)=>	open,
		mgt_headerlocked_o	(1)=>	mgt_headerlocked_s,
		mgt_headerlocked_o	(2)=>	mgt_headerlocked2_s,
-- Reconf.         --
-- De Remigis
		gbtbank_mgt_drp_rst	=>	'0',
		gbtbank_mgt_drp_clk	=>	clk_fr,
-- TX ctrl        --
-- De Remigis
		tx_encoding_sel_i	(1)=>	txencodingsel,
		tx_encoding_sel_i	(2)=>	txencodingsel,
		gbtbank_tx_isdata_sel_i	(1)=>	txisdatasel_from_user,
		gbtbank_tx_isdata_sel_i	(2)=>	txisdatasel_from_user,
-- De Remigis
-- "00" in port gbtbank_test_pattern_sel_i for real data
-- "01" in port gbtbank_test_pattern_sel_i for counter data
-- "10" in port gbtbank_test_pattern_sel_i for static data
		gbtbank_test_pattern_sel_i=>	"00",
-- RX ctrl      --
-- De Remigis
		rx_encoding_sel_i	(1)=>	rxencodingsel,
		rx_encoding_sel_i	(2)=>	rxencodingsel,
gbtbank_reset_gbtrxready_lost_flag_i(1)=>resetgbtrxreadylostflag_from_user,
gbtbank_reset_gbtrxready_lost_flag_i(2)=>resetgbtrxreadylostflag_from_user,
gbtbank_reset_data_errorseen_flag_i(1)=>resetdataerrorseenflag_from_user,
gbtbank_reset_data_errorseen_flag_i(2)=>resetdataerrorseenflag_from_user,
		gbtbank_rxframeclk_alignpatter_i=>debug_clk_alignment_debug,
		gbtbank_rxbitslit_rstoneven_i(1)=>rxbitsliprstoneven_from_user,
		gbtbank_rxbitslit_rstoneven_i(2)=>rxbitsliprstoneven_from_user,
-- TX Status    --
-- De Remigis
		gbtbank_link_ready_o	(1)=>	stat_link_ready,
		gbtbank_link_ready_o	(2)=>	open,
		gbtbank_tx_matchflag_o=>	stat_tx_match_flag,
		gbtbank_tx_aligned_o	(1)=>	txaligned_from_gbtbank,
		gbtbank_tx_aligned_o	(2)=>	open,
		gbtbank_tx_aligncomputed_o(1)=>txaligncomputed_from_gbtbank,
		gbtbank_tx_aligncomputed_o(2)=>open,
-- RX Status    --
-- De Remigis
		gbtbank_gbtrx_ready_o	(1)=>	gbtrxready_from_gbtexmpldsgn,
		gbtbank_gbtrx_ready_o	(2)=>	open,
		gbtbank_gbtrxready_lost_flag_o(1)=>stat_rx_gbt_ready_lost_flag,
		gbtbank_gbtrxready_lost_flag_o	(2)=>	open,
		gbtbank_rxdata_errorseen_flag_o	(1)=>	stat_rx_data_error_seen,
		gbtbank_rxdata_errorseen_flag_o	(2)=>	open,
		gbtbank_rxextradata_widebus_errorseen_flag_o	(1)=>	open,
		gbtbank_rxextradata_widebus_errorseen_flag_o	(2)=>	open,
		gbtbank_rx_matchflag_o	(1)=>	stat_rx_match_flag,
		gbtbank_rx_matchflag_o	(2)=>	open,
		gbtbank_rx_isdata_sel_o	(1)=>	rxisdata_from_gbtexmpldsgn,
		gbtbank_rx_isdata_sel_o	(2)=>	open,
		gbtbank_rx_errordetected_o	(1)=>	gbterrordetected,
		gbtbank_rx_errordetected_o	(2)=>	open,
		gbtbank_rx_bitmodified_flag_o	(1)=>	gbtmodifiedbitflag,
		gbtbank_rx_bitmodified_flag_o	(2)=>	open,
		rx_frameclk_rdy_o	(1)=>rxframeclkready_from_gbtexmpldsgn,
		rx_frameclk_rdy_o	(2)=>open,
	gbtbank_rxbitslip_rst_cnt_o(1)=>rxbitsliprstcount_from_gbtexmpldsgn,
		gbtbank_rxbitslip_rst_cnt_o	(2)=>	open,
-- XCVR ctrl    --
-- De Remigis
		gbtbank_loopback_i=>	loopback_from_user,
		gbtbank_tx_pol	(1)=>	'0',
		gbtbank_tx_pol	(2)=>	'0',
		gbtbank_rx_pol	(1)=>	'0',
		gbtbank_rx_pol	(2)=>	'0');     
	rxdata<=	rxdata_from_gbtexmpldsgn;
	rxframeclk<=	rxframeclk_from_gbtexmpldsgn;
	headerlock<=	mgt_headerlocked_s;
-- BER                                 --
--=====================================--
countWordReceivedProc: PROCESS(reset_from_genRst, rxframeclk_from_gbtExmplDsgn)
begin
  if reset_from_genRst = '1' then
    countWordReceived <= (others => '0');
    countBitsModified <= (others => '0');
    countWordErrors    <= (others => '0');

  elsif rising_edge(rxframeclk_from_gbtExmplDsgn) then
    if gbtRxReady_from_gbtExmplDsgn = '1' then
      if gbtErrorDetected = '1' then
        countWordErrors    <= std_logic_vector(unsigned(countWordErrors) + 1 );                
      end if;
      countWordReceived <= std_logic_vector(unsigned(countWordReceived) + 1 );
    end if;
    countBitsModified <= std_logic_vector(unsigned(modifiedBitsCnt) + unsigned(countBitsModified) );
  end if;
end process;

gbtModifiedBitFlagFiltered(127 downto 84) <= (others => '0');
gbtModifiedBitFlagFiltered(83 downto 0)   <= gbtModifiedBitFlag when gbtRxReady_from_gbtExmplDsgn = '1' else (others => '0');

countOnesCorrected: entity work.CountOnes
  Generic map (SIZE => 128, MAXOUTWIDTH => 8)
  Port map( 
    Clock => rxframeclk_from_gbtExmplDsgn,
    I     => gbtModifiedBitFlagFiltered,
    O     => modifiedBitsCnt
  );                              




--==============--   
-- Test control --   
--==============--
             
latOptGbtBankTx_from_gbtExmplDsgn                       <= '1';
latOptGbtBankRx_from_gbtExmplDsgn                       <= '1';
 
alignmenetLatchProc: process(txFrameClk_from_txPll)-- txFrameClk_from_txPll)
begin
  if reset_from_genRst = '1' then
    stat_tx_aligned <= '0';
  elsif rising_edge(txFrameClk_from_txPll) then -- txFrameClk_from_txPll) then
    if txAlignComputed_from_gbtbank = '1' then
      stat_tx_aligned <= txAligned_from_gbtbank;
    end if;
  end if;
end process;

--          * Note!! TX and RX DATA do not share the same ILA module (txIla and rxIla respectively) 
--            because when receiving RX DATA from another board with a different reference clock, the 
--            TX_FRAMECLK/TX_WORDCLK domains are asynchronous with respect to the RX_FRAMECLK/RX_WORDCLK domains.        
-- De Remigis
	txila:	xlx_k7v7_vivado_debug	port	map
		(clk=>	txframeclk_from_gbtexmpldsgn,
		probe0=>txdata_from_gbtexmpldsgn(83	downto	82),
		probe1=>txdata_from_gbtexmpldsgn(81	downto	80),
--8b10b support removed
		probe2=>	txdata_from_gbtexmpldsgn(79	downto	0),
		probe3	(0)=>	txisdatasel_from_user,
		probe3	(1)=>	ipb_clk,
		probe4=>	txdata_from_gbtexmpldsgn2(79	downto	0));
-- De Remigis
	rxila:	xlx_k7v7_vivado_debug	port	map
		(clk=>	rxframeclk_from_gbtexmpldsgn,
		probe0=>	rxdata_from_gbtexmpldsgn(83	downto	82),
		probe1=>	rxdata_from_gbtexmpldsgn(81	downto	80),
		probe2=>	rxdata_from_gbtexmpldsgn(79	downto	0),
		probe3	(0)=>	rxisdata_from_gbtexmpldsgn,
		probe3	(1)=>	mgt_headerlocked_s,
		probe4=>	rxdata_from_gbtexmpldsgn2(79	downto	0));
mgt_word_rev_gen: for i in 0 to 39 generate 
  mgt_rxword_rev_s(i) <= mgt_rxword_s(39-i);
  mgt_txword_rev_s(i) <= mgt_txword_s(39-i);
end generate;

---------------------------------- CONTROL -----------------------------------
    
    jtag_master_inst : entity work.jtagCtrl_gbtfpgaTest
      PORT MAP (
        aclk => txFrameClk_from_txPll, --clk40FromMgt_txwordclk_s, -- 
        aresetn => txFrameClk_lock_from_txPll,
        m_axi_awaddr => m_axi_awaddr,
        m_axi_awprot => m_axi_awprot,
        m_axi_awvalid => m_axi_awvalid,
        m_axi_awready => m_axi_awready,
        m_axi_wdata => m_axi_wdata,
        m_axi_wstrb => m_axi_wstrb,
        m_axi_wvalid => m_axi_wvalid,
        m_axi_wready => m_axi_wready,
        m_axi_bresp => m_axi_bresp,
        m_axi_bvalid => m_axi_bvalid,
        m_axi_bready => m_axi_bready,
        m_axi_araddr => m_axi_araddr,
        m_axi_arprot => m_axi_arprot,
        m_axi_arvalid => m_axi_arvalid,
        m_axi_arready => m_axi_arready,
        m_axi_rdata => m_axi_rdata,
        m_axi_rresp => m_axi_rresp,
        m_axi_rvalid => m_axi_rvalid,
        m_axi_rready => m_axi_rready
      );


    gbtsc_controller_inst: entity work.gbtsc_controller
      Port map( 
        -- AXI4LITE Interface
        tx_clock              => txFrameClk_from_txPll,
        meas_clock            => fast_clock_for_meas,
        
        -- AXI4LITE Interface
        S_AXI_ARESETN         => txFrameClk_lock_from_txPll,
        S_AXI_AWADDR          => m_axi_awaddr(4 downto 0),
        S_AXI_AWVALID         => m_axi_awvalid,
        S_AXI_AWREADY         => m_axi_awready,
        S_AXI_WDATA           => m_axi_wdata,
        S_AXI_WSTRB           => m_axi_wstrb,
        S_AXI_WVALID          => m_axi_wvalid,
        S_AXI_WREADY          => m_axi_wready,
        S_AXI_BRESP           => m_axi_bresp,
        S_AXI_BVALID          => m_axi_bvalid,
        S_AXI_BREADY          => m_axi_bready,
        S_AXI_ARADDR          => m_axi_araddr(4 downto 0),
        S_AXI_ARVALID         => m_axi_arvalid,
        S_AXI_ARREADY         => m_axi_arready,
        S_AXI_RDATA           => m_axi_rdata,
        S_AXI_RRESP           => m_axi_rresp,
        S_AXI_RVALID          => m_axi_rvalid,
        S_AXI_RREADY          => m_axi_rready,
                
        -- To GBT-SC
        reset_gbtsc         => reset_sca_jtag,
        start_reset         => start_reset_jtag,
        start_connect       => start_connect_jtag,
        start_command       => start_sca_jtag,           
                
        tx_address          => open,
        tx_transID          => tx_trid_jtag,
        tx_channel          => tx_ch_jtag,
        tx_len              => tx_len_jtag,
        tx_command          => tx_cmd_jtag,
        tx_data             => tx_data_jtag,           
        
        rx_reply_received_i => sca_rx_done,
        rx_address          => rx_addr,
        rx_transID          => stat_id,
        rx_channel          => stat_ch,
        rx_len              => stat_len,
        rx_error            => stat_error,
        rx_data             => stat_ec_data,
                       
        -- Data to GBT-IC State machine        
        ic_ready             => stat_ready,
        ic_empty             => stat_rx_empty,
        
        -- Configuration
        GBTx_address         => GBTx_address_to_gbtic_jtag,
        Register_addr        => Register_addr_to_gbtic_jtag,
        nb_to_be_read        => nb_to_be_read_to_gbtic_jtag,
        
        -- Control        
        start_write          => start_write_to_gbtic_jtag,
        start_read           => start_read_to_gbtic_jtag,
        
        -- WRITE register(s)
        data_o               => data_to_gbtic_jtag,
        wr                   => wr_to_gbtic_jtag,
        
        -- READ register(s)        
        data_i               => stat_ic_data,
        rd                   => rd_to_gbtic_jtag,
                
        rd_gbtx_addr         => stat_gbtx_addr,
        rd_mem_ptr           => stat_reg,
        rd_nb_of_words       => stat_nb,
        
         -- Status
         delay_cnter_o       => cmd_delay,
         wdt_error_o         => open,
         ready_o             => open
      );

-- gbtsc_intf register selects the interface to control gbtx/sca communication (1 = ipbus; 0 = jtag)
                  
reset_sca                   <= conf_reset_gbt_sc                    when conf_gbtsc_intf = '1' else reset_sca_jtag               when conf_gbtsc_intf = '0';                                                 
                                                                                                
GBTx_address_to_gbtic       <= conf_gbtx_addr                       when conf_gbtsc_intf = '1' else GBTx_address_to_gbtic_jtag   when conf_gbtsc_intf = '0'; 
Register_addr_to_gbtic      <= conf_reg                             when conf_gbtsc_intf = '1' else Register_addr_to_gbtic_jtag  when conf_gbtsc_intf = '0'; 
nb_to_be_read_to_gbtic      <= conf_nb_to_read                      when conf_gbtsc_intf = '1' else nb_to_be_read_to_gbtic_jtag  when conf_gbtsc_intf = '0'; 

wr_to_gbtic                 <= ctrl_stb(9)                          when conf_gbtsc_intf = '1' else wr_to_gbtic_jtag             when conf_gbtsc_intf = '0'; 

data_to_gbtic               <= conf_ic_data                         when conf_gbtsc_intf = '1' else data_to_gbtic_jtag           when conf_gbtsc_intf = '0'; 

rd_to_gbtic                 <= conf_read_fifo                       when conf_gbtsc_intf = '1' else rd_to_gbtic_jtag             when conf_gbtsc_intf = '0'; 

start_write_to_gbtic        <= ctrl_stb(8) and conf_ic_rd_wr        when conf_gbtsc_intf = '1' else start_write_to_gbtic_jtag    when conf_gbtsc_intf = '0'; 
start_read_to_gbtic         <= ctrl_stb(8) and not (conf_ic_rd_wr)  when conf_gbtsc_intf = '1' else start_write_to_gbtic_jtag    when conf_gbtsc_intf = '0';

start_reset                 <= conf_send_reset                      when conf_gbtsc_intf = '1' else start_reset_jtag             when conf_gbtsc_intf = '0'; 
start_connect               <= conf_send_connect                    when conf_gbtsc_intf = '1' else start_connect_jtag           when conf_gbtsc_intf = '0'; 

start_sca                   <= ctrl_stb(5)          when conf_gbtsc_intf = '1' else start_sca_jtag               when conf_gbtsc_intf = '0';         

tx_addr                     <= x"00";
tx_trid                     <= conf_id              when conf_gbtsc_intf = '1' else tx_trid_jtag                 when conf_gbtsc_intf = '0'; 
tx_ch                       <= conf_ch              when conf_gbtsc_intf = '1' else tx_ch_jtag                   when conf_gbtsc_intf = '0'; 
tx_cmd                      <= conf_comm            when conf_gbtsc_intf = '1' else tx_cmd_jtag                  when conf_gbtsc_intf = '0'; 
tx_data                     <= conf_ec_data         when conf_gbtsc_intf = '1' else tx_data_jtag                 when conf_gbtsc_intf = '0'; 


-- the stat_reply_flag must be updated with the value of sca_rx_done (asserted after a SCA read/write opertation. 
-- Also the stat_reply_flag must be reset after it is read.
-- reset signal syncronization needed between ipbus/gbt clock domains.    

process(ipb_clk)
    begin   
        if rising_edge(ipb_clk) then
            if ipb_in.ipb_strobe = '1' and ipb_in.ipb_write = '0' and ipb_in.ipb_addr(3 downto 0)= "0110" then
                reset_reply_flag_s     <= '1';
            else
                reset_reply_flag_s     <= '0';
            end if;        
        end if;
end process;

process(txFrameClk_from_txPll)
    begin
        if rising_edge(txFrameClk_from_txPll) then
            reset_reply_flag_f1 <= reset_reply_flag_s;
            reset_reply_flag_f2 <= reset_reply_flag_f1;
            reset_reply_flag_and <= not(reset_reply_flag_f1) and reset_reply_flag_f2;
        end if;
end process;


process(txFrameClk_from_txPll) is
	begin
        if (rising_edge(txFrameClk_from_txPll)) then
            if reset_reply_flag_and = '1' then
                stat_reply_flag <= '0';
            else
                if sca_rx_done = '1' then
                    stat_reply_flag <= '1';
                end if;
            end if;
        end if;
end process;

-- module for gbtx-sca communication. Inputs/outputs could come from jtag or ipbus interface
                                                                               
gbtsc_inst: entity work.gbtsc_top
generic map(
  -- IC configuration
  g_IC_COUNT          => 1,
  g_IC_FIFO_DEPTH     => 1,
  
  -- EC configuration
  g_SCA_COUNT         => 1
)
port map(
  -- Clock & reset
  tx_clk_i                => txFrameClk_from_txPll,
  rx_clk_i                => txFrameClk_from_txPll,
  reset_i                 => reset_sca,
  
  -- IC configuration        
  tx_GBTx_address_i(0)    => GBTx_address_to_gbtic,
  tx_register_addr_i(0)   => Register_addr_to_gbtic,
  tx_nb_to_be_read_i(0)   => nb_to_be_read_to_gbtic,
  
  -- IC Status
  tx_ready_o(0)           => stat_ready,
  rx_empty_o(0)           => stat_rx_empty,
  
  rx_gbtx_addr_o(0)       => stat_gbtx_addr,
  rx_mem_ptr_o(0)         => stat_reg,
  rx_nb_of_words_o(0)     => stat_nb,
      
  -- IC FIFO control
  tx_wr_i(0)              => wr_to_gbtic,
  tx_data_to_gbtx_i(0)    => data_to_gbtic,
  
  rx_rd_i(0)              => rd_to_gbtic,
  rx_data_from_gbtx_o(0)  => stat_ic_data,
  
  -- IC control
  ic_enable_i             => "1",
  tx_start_write_i        => start_write_to_gbtic,
  tx_start_read_i         => start_read_to_gbtic,
      
  -- SCA control
  sca_enable_i            => "1",
  start_reset_cmd_i       => start_reset,
  start_connect_cmd_i     => start_connect,
  start_command_i         => start_sca,
  inject_crc_error        => '0',
  
  -- SCA command
  tx_address_i            => tx_addr,
  tx_transID_i            => tx_trid,
  tx_channel_i            => tx_ch,
  tx_command_i            => tx_cmd,
  tx_data_i               => tx_data,
  
  rx_received_o(0)        => sca_rx_done,
  rx_address_o(0)         => rx_addr,
  rx_control_o(0)         => open,
  rx_transID_o(0)         => stat_id,
  rx_channel_o(0)         => stat_ch,
  rx_len_o(0)             => stat_len,
  rx_error_o(0)           => stat_error,
  rx_data_o(0)            => stat_ec_data,

  -- EC line
  ec_data_o(0)            => txData_to_gbtExmplDsgn(81 downto 80),
  ec_data_i(0)            => rxData_from_gbtExmplDsgn(81 downto 80),
  
  -- IC lines
  ic_data_o(0)            => txData_to_gbtExmplDsgn(83 downto 82),
  ic_data_i(0)            => rxData_from_gbtExmplDsgn(83 downto 82)
);     
   
end structural;