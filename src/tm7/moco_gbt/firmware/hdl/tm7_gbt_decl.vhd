----------------------------------------------------------------------------------
-- TM7-GBT data types
-- Javier Sastre, CIEMAT, 2019
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

package tm7_gbt_decl is

    constant AXI4LITE	: integer := 0;
    constant IPBUS		: integer := 1;
	
    constant INTERFACE : integer := IPBUS;

end tm7_gbt_decl;
