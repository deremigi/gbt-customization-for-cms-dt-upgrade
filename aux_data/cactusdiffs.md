This file explains the relationship of the /cactusupgrades folder to the cactusupgrades folder of the mp7 project at https://gitlab.cern.ch/cms-cactus/firmware/mp7 and the ipbus sources at https://github.com/ipbus/firmware

This version of the TM7 fw is based on the 2.4.0 tag of the mp7 project.

/src/tm7/tm7_common
/src/tm7/moco_top

These folders contain some files that were originally on cactusupgrades, but were moved to modify out of place. They should be marked with the original route at the top of the file.

/src/cactusupgrades/boards/mp7/

every file in this folder is directly copied from its original mp7 file.

/src/cactusupgrades/components/

The ipbus and mp7 modules should be identical to the ones either at the mp7 repo tag on which this fw is based, or the ipbus github tag in which the mp7 tag is based. This is not, however true for all modules, and here comes a list of the changes to take into account:

- mp7_ttc/ we have modified the clocks so there are changes to 2 files here (mp7_ttc.vhd, ttc_clocks.vhd) and also have changed the BGOs (mp7_ttc_decl.vhd).

- mp7_infra/ many modifications to mp7_infra.vhd. One of them is that now the decoding of the top-level ipbus branches is done at top instead of here.


